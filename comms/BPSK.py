#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
import scipy.signal


# plotting function
def plot_constellation(encoded_sequence):
	plt.figure()
	plt.plot(np.real(encoded_sequence), np.imag(encoded_sequence), 'b*')
	plt.show()

def plot_signal(t, sig):
	plt.figure()
	plt.plot(sig)
	
	
# binary / constellations
def generate_random_binary(L):
	return np.random.randint(0, 2, L)

def encode_BPSK(bin_sequence):
	return np.where(bin_sequence == 1, 1, -1)

def decode_BPSK(symbols_seq):
	return np.where(symbols_seq > 0, 1, 0)

def count_errors(seq1, seq2):
	return np.sum(seq1 != seq2)


# filter functions
def generate_SRRC(N, alpha, Ts, Fs):
	"""
	Ts : symbol duration [s]
	F  : samples per symbol
	N  : duration of the impulse response [Ts*s]
	alpha : roll-off factor
	"""
	t = np.arange(-Ts*N/2,Ts*N/2,Ts/F)
	
	# general case
	y = (1/np.sqrt(F)) * (1 / (1-(4*alpha*t/Ts)**2)) * ((1-alpha)*np.sinc((t/Ts)*(1-alpha))+(4*alpha/np.pi) * np.cos((np.pi*t/Ts)*(1+alpha)));
	
	# Particular values (t=0 and abs(t) = T/(4*alpha))
	for i, t_i in enumerate(t):
		if abs(t[i]) < 1e-17:
			y[i] = (1/np.sqrt(F))*(1-alpha+4*(alpha/np.pi))
		
		if alpha != 0 and abs(abs(t[i]) - Ts/(4*alpha)) < 10^(-17):
			y[i] = (alpha/np.sqrt(2*F))*((1+2/np.pi)*np.sin(np.pi/(4*alpha)) + (1-2/np.pi)*np.cos(np.pi/(4*alpha)))
	return t,y

def modulate(symbols, mod_filter):
	ct = np.zeros(len(symbols)*F)
	for i, c_i in enumerate(symbols):
		ct[i*F] =  c_i
	
	t = np.linspace(0, len(symbols)*Ts, len(symbols))
	signal = scipy.signal.convolve(ct, mod_filter)
	return t, signal

def demodulate(signal, mod_filter):
	matched_filter = np.conj(mod_filter[::-1])
	demodulated = scipy.signal.convolve(signal, matched_filter)
	
	return demodulated[N*F-1:-N*F:F]

def generate_noise(signal, EbN0dB):
	
	EbN0 = 10**(EbN0dB/10)
	P_n = np.var(signal)/(2*EbN0)
	noise = np.sqrt(P_n) * np.random.randn(len(signal))
	
	return noise + signal




# binary rate (bits/s)
Rb = 9600;

# temps symbole (BPSK, 1 bit = 1 sybmole)
Ts = 1.0/Rb

# samples per symbols: echantillonage
F = 16

# filter impulse response duration (1 pulse = N * Ts seconds)
# if N is bigger, more precise demodulation
# in practice, N = 8 is near enough
N = 8



roll_off = 1
t_filter, srrc = generate_SRRC(N, roll_off, Ts, F)

# test encoding
test_sequence = generate_random_binary(1000)
symbols = encode_BPSK(test_sequence)

t, raw_signal = modulate(symbols, srrc)
noisy_signal = generate_noise(raw_signal, -10)

# decode raw signal
received_symbols = demodulate(noisy_signal, srrc)
received_binary = decode_BPSK(received_symbols)

# Bit-Error Rate: proportion of errors
BER = count_errors(test_sequence, received_binary)/len(test_sequence)
print('BER:', BER)

# plot constellations
plt.figure()
plt.scatter(np.real(symbols), np.imag(symbols), label='emitted symbols', facecolors='none', edgecolors='b', s=80)
plt.scatter(np.real(received_symbols), np.imag(received_symbols), label='received symbols', color='r', marker='+')

# plot binary
plt.figure()
plt.plot(test_sequence, "bo", label='emitted binary', ms=10)
plt.plot(received_binary, "ro", label='received binary')

plt.figure()
plt.plot(np.linspace(0, len(symbols)*Ts, (len(symbols)+N)*F-1), raw_signal)

plt.show()
