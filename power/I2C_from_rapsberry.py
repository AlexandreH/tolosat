#!/usr/bin/python3

import time
import random
from smbus2 import SMBus, i2c_msg
import numpy as np

# address on the I2C bus
I2C_CHANNEL = 0
I2C_ADDRESS = 23# = 0x17

DEBUG = 1

# helper functions
def print_binary(bytes_array):
	print([np.binary_repr(b, 8) for b in bytes_array])
	
def send_gomspace_command(answer_size, *cmd):
	if DEBUG:
		print("binary command: ")
		print_binary(cmd)
		return cmd[0], -1, [0]*answer_size
	else:
		bus.i2c_rdwr(i2c_msg.write(I2C_ADDRESS, cmd))
		time.sleep(0.01)
		
		response = i2c_msg.read(I2C_ADDRESS, answer_size+2)
		bus.i2c_rdwr(response)
		#print([(b & 0xff) for b in response])
		data = [b for b in response]
		return data[0], data[1], data[2:]

def send_cmd_no_answer(*cmd):
	if DEBUG:
		print("binary command (no answer): ")
		print_binary(cmd)
	else:
		bus.write_i2c_block_data(I2C_ADDRESS, cmd[0], cmd[1:])

def read_int(bytes_array, offset, size=1, signed=False):
	if offset+size > len(bytes_array):
		return None
	
	res = 0
	for i in range(offset, offset+size):
		res += bytes_array[i] << 8*(offset+size-i-1)
	res &= (1 << 8*size) -1
	
	if signed:
		if res & (1 << (8*size-1)) == (1 << (8*size-1)):
			res -= (1 << 8*size)
	return res

def read_array(bytes_array, offset, array_elements, element_size=1, signed=False):
	return [read_int(bytes_array, offset + i*element_size, element_size, signed) for i in range(array_elements)]


# P31u commands

PORT_P31U_PING = 0x01
PORT_P31U_GET_HK = 0x08
PORT_P31U_REBOOT = 0x04
PORT_P31U_SET_OUTPUT = 0x09
PORT_P31U_SET_SINGLE_OUTPUT = 0x0a
PORT_P31U_SET_PV_VOLT = 0x0b
PORT_P31U_SET_PV_AUTO = 0x0c
PORT_P31U_SET_HEATER = 0x0d
PORT_P31U_RESET_COUNTERS = 0x0f
PORT_P31U_RESET_WDT = 0x10
PORT_P31U_CONFIG_CMD = 0x11
PORT_P31U_CONFIG_GET = 0x12
PORT_P31U_CONFIG_SET = 0x13
PORT_P31U_HARD_RESET = 0x14
PORT_P31U_CONFIG2_CMD = 0x15
PORT_P31U_CONFIG2_GET = 0x16
PORT_P31U_CONFIG2_SET = 0x17
PORT_P31U_CONFIG3 = 0x19

def p31u_reset_counters():
	send_cmd_no_answer(PORT_P31U_RESET_COUNTERS, 0x42)
	
def p31u_reset_wdt():
	send_cmd_no_answer(PORT_P31U_RESET_WDT, 0x78)
	
def reboot_p31u():
	send_cmd_no_answer(PORT_P31U_REBOOT, 0x80, 0x07, 0x80, 0x07)

def ping_p31u():
	val = random.randint(0,255)
	cmd, err, dat = send_gomspace_command(1, PORT_P31U_PING, val)
	return dat[0] == val

def p31u_set_single_output(channel, value, delay):
	# delay = from 0 to 32767
	# to convert to int16 format 
	int16_delay = delay
	if delay > 0x7fff:
		int16_delay = 0x7fff
	if delay < 0:
		int16_delay = 0
	# the int16_delay is now a python number, with sign ?, "saturated"
	
	send_cmd_no_answer(PORT_P31U_SET_SINGLE_OUTPUT, (channel & 0xff), (1 if value else 0), (int16_delay >> 8) & 0xff, int16_delay & 0xff)

def p31u_set_output(*channels):
	if len(channels) != 6:
		print("6 channels are expected !")
		return None
	
	bin_param = 0;
	for i, val in enumerate(channels):
		if val:
			bin_param += 1 << i
			
	send_cmd_no_answer(PORT_P31U_SET_OUTPUT, bin_param)

def get_hk1_p31u():
	cmd, err, bits = send_gomspace_command(43, PORT_P31U_GET_HK)
	# example (real data)
	#bits = [8, 0, 1, 114, 1, 105, 1, 113, 0, 0, 27, 202, 0, 15, 0, 23, 0, 23, 0, 23, 0, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 116, 0, 0, 2, 252][2:]
	data = {
		'pv': read_array(bits, 0, 3, 2, False),
		'pc': read_int(bits, 6, 2, False),
		'bv': read_int(bits, 8, 2, False),
		'sc': read_int(bits, 10, 2, False),
		'temp': read_array(bits, 12, 4, 2, True),
		'batt_temp': read_array(bits, 20, 2, 2, True),
		'latchup': read_array(bits, 24, 6, 2, False),
		'reset': read_int(bits, 36, 1, False),
		'bootcount': read_int(bits, 37, 2, False),
		'sw_errors': read_int(bits, 39, 2, False),
		'ppt_mode': read_int(bits, 41, 1, False),
		'channel_status': read_int(bits, 42, 1, False),
	}
	return data

def get_hk_p31u():
    cmd = i2c_msg.write(I2C_ADDRESS, [PORT_P31U_GET_HK])
    bus.i2c_rdwr(cmd)
    time.sleep(1)

    response = i2c_msg.read(I2C_ADDRESS, 45)
    bus.i2c_rdwr(response)
    
    recv_val = None
    for recv_val in response:
        print(recv_val)


bus = SMBus(I2C_CHANNEL)
print(ping_p31u())
#reboot_p31u()
print(get_hk1_p31u())
p31u_set_output(1, 0, 1, 0, 1, 0)

p31u_set_single_output(3, True, 10)
bus.close()
